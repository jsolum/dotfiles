return {
	"junegunn/fzf",
	build = ":call fzf#install()",
	dependencies = { "junegunn/fzf.vim" },
	config = function()
		vim.keymap.set("n", "<leader>p", "<cmd>GFiles<cr>", { silent = true, noremap = true, desc = "Find Git files" })
		vim.keymap.set("n", "<leader>sf", "<cmd>Files<cr>", { silent = true, noremap = true, desc = "Find files" })
		vim.keymap.set(
			"n",
			"<leader>sr",
			"<cmd>Registers<cr>",
			{ silent = true, noremap = true, desc = "Find Registers" }
		)
		vim.keymap.set("n", "<leader>sm", "<cmd>Marks<cr>", { silent = true, noremap = true, desc = "Find Marks" })
		vim.keymap.set("n", "<leader>sw", function()
			local word = vim.fn.expand("<cword>")
			vim.cmd("Rg " .. word)
		end)
		vim.keymap.set("n", "<leader>sW", function()
			local word = vim.fn.expand("<cWORD>")
			vim.cmd("Rg " .. word)
		end)
	end,
}
