#!/bin/bash
# Adds files to shell startup file to ensure everything is sourced on load

# Vars
RC_SHELL=testrc

# parse args
args+=()
while (( $# > 0 ))
do
    case "$1" in
        --shell | -s)
            RC_SHELL="$2"
            shift
            ;;
        --file | -f)
            SELECTED_RC="$2"
            shift
            ;;
        --backup | -b)
            SELECTED_BACKUP_DIRECTORY="$2"
            shift
            ;;
        --help | -h)
            echo "Usage: $0 [OPTION]... [DIRECTORY]..."
            echo "   -s, --shell         what kind of shell file to add sources to e.g. zshrc, bashrc, etc. By default it's zshrc"
            echo "   -b, --backup        defaults to \$HOME/dot_backup/"
            echo "   -f, --file          defaults to \$HOME/.<shell file>. Recommended you don't override but it's there if you need"
            echo "   -h, --help"
            exit
            ;;
        *)
            args+=("$1")
            ;;
    esac
    shift
done
DEFAULT_RC=$HOME/.$RC_SHELL
DEFAULT_BACKUP_DIRECTORY=$HOME/dot_backup

BACKUP_NAME=$RC_SHELL\_$(date +"%m-%d-%Y_%T")
RC=${SELECTED_RC:-$DEFAULT_RC}
BACKUP_DIRECTORY=${SELECTED_BACKUP_DIRECTORY:-$DEFAULT_BACKUP_DIRECTORY}
AUTO_GENERATED_LINE="####### Auto Generated #######"

if [ -f $RC ]; then
    mkdir -p $BACKUP_DIRECTORY
    cp $RC $BACKUP_DIRECTORY/$BACKUP_NAME
    echo "⚡ Backed up current $RC to $BACKUP_DIRECTORY/$BACKUP_NAME"
else
    touch $RC
    echo "✨ Created $RC"
fi

#### Source dependencies
if [ $(grep -Fc "$AUTO_GENERATED_LINE" $RC) -eq 0 ]; then
    echo "💎 Setting up fresh install"
    echo $AUTO_GENERATED_LINE >> $RC
fi

echo && echo "Adding files:"
for folder in "${args[@]}"
do
    for file in $(ls $folder)
    do
        if [ $(grep -Fc "$file" $RC) -gt 0 ]; then
            echo "Skipping: $file"
        elif [ ${file: -2} == "sh" ]; then
            echo "source ~/.$file" >> $RC
            echo "✅ Added: $file, to $RC"
        fi
    done
done
