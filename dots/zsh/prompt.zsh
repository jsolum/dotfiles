# Enable substitution in the prompt.
setopt prompt_subst


# Set the prompt.
PROMPT='%F{blue}%2~%f %F{cyan}❯%f '  # Minimal prompt icon on new line for typing.

# Right prompt for time and exit status.
RPROMPT='%F{green}$(current_branch)%f %F{cyan}%*%f %F{red}%?%f'  # Time and exit status (red if non-zero)

