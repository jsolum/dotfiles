#if [ ${shell: -4} == "bash" ]; then
#  # Bash Completion
#  if [ -f `brew --prefix`/etc/bash_completion ]; then
#    . `brew --prefix`/etc/bash_completion
#  fi
#
#  # Iterm2 integration
#  test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"
#fi

# Rust
source "$HOME/.cargo/env"

# Jdk
#export PATH="/usr/local/opt/openjdk/bin:$PATH"

# Java home
# https://github.com/halcyon/asdf-java#java_home
. ~/.asdf/plugins/java/set-java-home.zsh


# Adds zsh syntax highlighting
source $(brew --prefix 2> /dev/null)/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2> /dev/null
source $(brew --prefix 2> /dev/null)/share/zsh-autosuggestions/zsh-autosuggestions.zsh 2> /dev/null

