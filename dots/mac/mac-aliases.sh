alias ls='ls -GFh' 
alias ll='ls -lG' 
alias reset="osascript -e 'tell application \"System Events\" to tell process \"Terminal\" to keystroke \"k\" using command down'" # Because mac doesn't have reset by default
alias lightMode="echo -e \"\033]50;SetProfile=Light\a\""
alias darkMode="echo -e \"\033]50;SetProfile=Dark\a\""
