alias staging10='ssh web1.us00.staging10'
alias staging9='ssh web1.us00.staging9'
alias perfcheck='ssh web1.us00.perf-check.production.rnd'
alias prod='aws-ssh ssh --trekker procore --stage production --role web'
alias prod_us02='aws-ssh ssh --trekker procore --stage production-us02 --role web'
alias staging1='aws-ssh ssh --trekker procore --stage staging1 --role web'
alias be='bundle exec'
alias rspec='be rspec'
alias rs='rspec'

alias dev_db='PGPASSWORD=A_GOOD_PASSWORD psql -U procore_db -d procore_development'


# Sandbox
# https://procoretech.atlassian.net/wiki/spaces/INF/pages/471531543/API+Sandbox
alias sandbox_us01='aws-ssh ssh --trekker procore --stage production-us01 --profile prnd_production --role web'
alias sandbox_us02='aws-ssh ssh --trekker procore --stage production-us02 --profile prnd_production --role web'
alias monthly_sandbox_us01='aws-ssh ssh --trekker procore --stage rnd_sbx-m-a --profile prnd_production --role web'
alias monthly_sandbox_us02='ssh web1.us02.procore.sbx-m-a.rnd'
alias pentest='ssh utility1.us00.procore.pentest.stg'
alias seed_sandbox='ssh web1.us00.seed-sandbox.procoretech.com'

alias tugboat="docker run -v $HOME/.procore:/etc/tugboat -it containers.artifacts.procoretech.com/developer-tooling/tugboat-ruby"
