export HOMEBREW_GITHUB_API_TOKEN=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

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

# Add Yarn bin for seedmgr
export PATH="$PATH:$(yarn global bin)" 2> /dev/null

# Add Deno
export PATH="$PATH:$HOME/.deno/bin"

alias stop-vpn="launchctl unload /Library/LaunchAgents/com.paloaltonetworks.gp.pangp*"
alias start-vpn="launchctl load /Library/LaunchAgents/com.paloaltonetworks.gp.pangp*"
