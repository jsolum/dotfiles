load_staging_data() {
    #set -euo pipefail

    # Prerequisites
    # * You must have access to SSH onto the staging servers.
    #
    # About
    #   This is designed to download the existing procore db dumps from the seed db server
    #   and run pg_restore locally. It uses SSH to list the files in the `/data/procore/pg_backups/dumps`
    #   directory to find the latest dump. If the db dump file is not found locally then we need to
    #   secure copy the file down before running pg_restore.
    #
    # Note
    # * Tugboat uses these dumps on our seed database to seed that database env.
    # * pg_restore does take a while to run.

    #SSH_USERNAME=jsolum
    #SEED_DB_HOST=db1.us00.staging9.procoretech.com
    #DUMP_DIR=/data/procore/pg_backups/dumps

    #DB_USER=procore_db
    #DB_NAME=procore_development

    #echo "Searching for a .dmp file in the ${DUMP_DIR} on ${SEED_DB_HOST}"
    #PROCORE_PG_DUMP_FILE=$(ssh $SSH_USERNAME@$SEED_DB_HOST ls $DUMP_DIR | grep -v tugboat | grep .dmp | grep -v login | grep $(date +%Y%m%d) | tail -1)

    #REMOTE_FILE="${DUMP_DIR}/${PROCORE_PG_DUMP_FILE}"
    #FILE="/tmp/${PROCORE_PG_DUMP_FILE}"
    #echo "Found pg_dump file: ${REMOTE_FILE}"

    #if [[ ! -f $FILE ]]; then
    #echo "Running scp of ${REMOTE_FILE} to ${FILE}"
    #scp $SSH_USERNAME@$SEED_DB_HOST:$DUMP_DIR/$PROCORE_PG_DUMP_FILE $FILE
    #echo "Finished scp of ${REMOTE_FILE} to ${FILE}"
    #fi

    #echo "Running pg_restore."
    #pg_restore --verbose --clean --no-acl --no-owner -j 20 -h localhost -U $DB_USER -d $DB_NAME $FILE
    #echo "Finished pg_restore."
    echo "This doesn't work anymore! Go to https://procoretech.atlassian.net/wiki/spaces/DEV/pages/1892909455/Load+seed+db+to+your+local+postgres"
    open 'https://procoretech.atlassian.net/wiki/spaces/DEV/pages/1892909455/Load+seed+db+to+your+local+postgres'
}

create_pr() {
        gh pr create -T budget.md
}

ruboc() {
    rubocop --autocorrect $(git diff origin/master... --name-only --diff-filter=ACM | grep rb$)
}

rubo_staged() {
    rubocop --auto-correct $(git diff --staged --name-only)
}

gh_open() {
    # Try to get the PR number for the current branch
    pr_number=$(gh pr view --json number -q .number 2>/dev/null)
    
    if [ -n "$pr_number" ]; then
        # If PR exists, open it
        gh pr view --web
    else
        # If no PR exists, open the current branch
        gh browse
    fi
}

compare() {
    gh_open
}
