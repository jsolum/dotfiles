#!/bin/bash

# Setup Bash Command Prompt
parse_current_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export PS1=" [\h] \[\033[36m\]\u \[\033[33m\]\w\[\033[32m\]\$(parse_current_branch)\[\033[00m\] $ " # Prompt w/ pwd and git branch
