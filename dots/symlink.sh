#!/bin/bash
#  Setup Dots
#  Pass in the folders you wish to sym link as args

backup=~/dotfile_backup

for folder in "$@"
do
    for file in $(ls $folder) 
    do
        if [ -f ~/.$file ]; then
            echo "  Backing up ~/.$file to $backup"
            mkdir -p $backup; mv ~/.$file $backup/$file
        fi

        echo "🔗 Linking $file"
        ln -sf "$(pwd)/$folder/$file" ~/.$file
    done
done
