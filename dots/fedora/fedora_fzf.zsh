source /usr/share/fzf/shell/key-bindings.zsh

# https://src.fedoraproject.org/rpms/fzf
#
# There seems to be a bug that the completion.sh script doesn't get loaded
# https://bugzilla.redhat.com/show_bug.cgi?id=1513913
source /usr/share/zsh/site-functions/fzf
