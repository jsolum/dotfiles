# Sets alt-tab to include windows from all workspaces
# source: https://www.reddit.com/r/pop_os/comments/pb0dnj/how_to_change_alttab_behavior_to_only_select_from/
gsettings set org.gnome.shell.window-switcher current-workspace-only false
