To configure the dot files run:
1. `./symlink.sh <folders you want to include>`
    a. This will symlink all the files in each folder you specified to the home directory
2. `./add_sources.sh <folders you want to include>`
    a. This will append sources to the ~/. files if the script ends with a `.sh`

What shell it installs for is defined as $RC in each script. 
Currently it defaults to zsh, so if you want to use bash you will have to edit that variable in the scripts 
