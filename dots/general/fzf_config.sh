#!/bin/bash

export FZF_COMPLETION_TRIGGER=','

# Setup fzf
# ---------
if [[ ! "$PATH" == */usr/local/opt/fzf/bin* ]]; then
  export PATH="$PATH:/usr/local/opt/fzf/bin"
fi

# FZF config

# Use ripgrep for fzf
# --files: List files that would be searched but do not search
# --no-ignore: Do not respect .gitignore, etc...
# --hidden: Search hidden files and folders
# --follow: Follow symlinks
# --glob: Additional conditions for search (in this case ignore everything in the .git/ folder)
export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow --glob "!.git/*"'
#export FZF_DEFAULT_COMMAND='
  #(git ls-tree -r --name-only HEAD ||
   #find . -path "*/\.*" -prune -o -type f -print -o -type l -print |
      #sed s/^..//) 2> /dev/null'

# fe [FUZZY PATTERN] - Open the selected file with the default editor
#   - Bypass fuzzy finder if there's only one match (--select-1)
#   - Exit if there's no match (--exit-0)
fe() {
  local files
  IFS=$'\n' files=($(fzf-tmux --query="$1" --multi --select-1 --exit-0 --preview 'head -100 {}'))
  [[ -n "$files" ]] && ${EDITOR:-vim} "${files[@]}"
}

# fbr - checkout git branch
fo() {
  local branches branch
  branches=$(git branch --sort=-committerdate) &&
  branch=$(echo "$branches" | fzf-tmux +m --height 10%) &&
  git checkout $(echo "$branch" | awk '{print $1}' | sed "s/.* //")
}

# For fzf stuff
[ -f ~/.fzf.sh ] && source ~/.fzf.sh
