export EDITOR=$(which nvim)

# Rust Cargo Config
#export PATH="$HOME/.cargo/bin:$PATH" # Add cargo

# Pyenv Config
export PYENV_ROOT="$HOME/.pyenv" 
export PATH="$PYENV_ROOT/bin:$PATH"
if command -v pyenv &> /dev/null; then
  eval "$(pyenv init -)"
fi

# Pipenv
export PIPENV_VENV_IN_PROJECT=1
export PIPENV_PYTHON=$PYENV_ROOT/shims/python

# Set Vi Mode
set -o vi
#bind 'set show-mode-in-prompt on' # needs updated bash if on mac

# Ruby Version Manager
#[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
#export PATH="$PATH:$HOME/.rvm/bin"

# NVM config
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
#
HISTCONTROL=ignoreboth

# Open a file and optionally highlight lines in GitHub using GitHub CLI
open_in_github() {
    local filename
    local line
    local branch

    while [[ $# -gt 0 ]]; do
        case "$1" in
            -f|--filename)
                filename="$2"
                shift 2
                ;;
            -l|--line)
                line="$2"
                shift 2
                ;;
            -b|--branch)
                branch="$2"
                shift 2
                ;;
            *)
                echo "Unknown argument: $1"
                return
                ;;
        esac
    done
    echo $branch

    if [[ -z "${branch}" ]]; then
            branch=$(git rev-parse --abbrev-ref HEAD 2> /dev/null)
    fi

    local args=""
    if [[ -n "${filename}" ]]; then
            if [[ -n "${line}" ]]; then
                    filename="$filename:${line}"
            fi

            echo $filename
            gh browse ${filename} -b $branch
    else
            echo "Usage: open_in_github <filename> [line_start]"
    fi
}



