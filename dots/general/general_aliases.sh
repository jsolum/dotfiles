# Aliases
alias rm='rm -i' # prompt before deleting
alias c='clear'
alias grep="grep --color"
alias ..='cd ../'
alias ...='cd ../../'

# Git Aliases
alias g='git' # git alias
alias gps='git push'
alias gpl='git pull'
alias grh='git reset --hard'
alias gco='git checkout'
alias gpf='git push --force-with-lease'
alias gca='git commit --amend'
alias gap='git add -p'
alias gcp='git commit --amend --no-edit && git push --force-with-lease'
alias gsc='npx git-cz' # semantic commit helper https://github.com/commitizen/cz-cli

alias vim='nvim'
alias ll='ls -lF --color=auto'

alias start_nvm='[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"' # This loads nvm

alias bookends_psql='psql -h localhost -p 5432 -U bookends_app -d bookends_development'
